import os
import glob

if len(glob.glob("inputs/*R2.fastq.gz")) >= 1:
	print("R2 reads found, assuming paired end input files")

	read1 = glob.glob("inputs/*R1.fastq.gz")
	read2 = glob.glob("inputs/*R2.fastq.gz")

	readBasename = []
	for read in read1:
		readBasename.append(os.path.basename(read.rsplit('.', -1)[0]).replace("R1", ""))

	rule all:
		input:
			expand("inputs/{bname}R1.fastq.gz", bname=readBasename),
			expand("inputs/{bname}R2.fastq.gz", bname=readBasename),
			"output/multiqc/",

	rule reformat_pe:
		input:
			r1 = "inputs/{bname}R1.fastq.gz",
			r2 = "inputs/{bname}R2.fastq.gz",
		output:
			flag = temp(touch("output/reformat/{bname}flag")),
			out1 = "output/reformat-{bname}/{bname}R1.reformat.fastq.gz",
			out2 = "output/reformat-{bname}/{bname}R2.reformat.fastq.gz",
		log:
			"output/logfiles/reformat-{bname}.log"
		priority: 1
		shell:
			"reformat.sh \
			in1={input.r1} \
			in2={input.r2} \
			vpair \
			out1={output.out1} \
			out2={output.out2} &> >(tee {log})"

	rule fastqc1_pe:
		input:
			flag = rules.reformat_pe.output.flag,
			r1 = "inputs/{bname}R1.fastq.gz",
			r2 = "inputs/{bname}R2.fastq.gz",
		output:
			dir = directory("output/pre_screen-{bname}"),
			flag = temp(touch("output/pre_screen-{bname}/flag")),
		log:
			"output/logfiles/{bname}-fastqc1.log"
		threads:
			4
		shell:
			"fastqc \
			-t {threads} \
			-o {output.dir} \
			{input.r1} \
			{input.r2} &> >(tee {log})"

	rule fastqscreen_pe:
		input:
			flag = rules.reformat_pe.output.flag,
			r1 = "inputs/{bname}R1.fastq.gz",
			r2 = "inputs/{bname}R2.fastq.gz",
		output:
			dir = directory("output/fastq_screen-{bname}"),
			r1 = "output/fastq_screen-{bname}/{bname}R1.tagged_filter.fastq.gz",
			r2 = "output/fastq_screen-{bname}/{bname}R2.tagged_filter.fastq.gz",
		log:
			"output/logfiles/{bname}-fastq_screen.log"
		threads:
			4
		shell:
			"fastq_screen \
			--nohits \
			--threads {threads} \
			--conf /resources/fastq_screen.conf \
			--outdir {output.dir} \
			{input.r1} {input.r2} &> >(tee {log})"

	rule repair_pe:
		input:
			r1 = rules.fastqscreen_pe.output.r1,
			r2 = rules.fastqscreen_pe.output.r2,
		output:
			touch("output/repair-{bname}.flag"),
			r1 = "output/repair-{bname}/fastq-{bname}R1.desensitized.fastq.gz",
			r2 = "output/repair-{bname}/fastq-{bname}R2.desensitized.fastq.gz",
		log:
			"output/logfiles/{bname}-repair.log"
		shell:
			"repair.sh in={input.r1} in2={input.r2} out1={output.r1} out2={output.r2} &> >(tee {log})"

	rule fastqc2_pe:
		input:
			r1 = rules.repair_pe.output.r1,
			r2 = rules.repair_pe.output.r2,
		output:
			dir = directory("output/post_screen-{bname}/{bname}"),
			flag = temp(touch("output/post_screen-{bname}/{bname}/flag")),
		log:
			"output/logfiles/{bname}-fastqc2.log"
		threads:
			4
		shell:
			"fastqc \
			-t {threads} \
			-o {output.dir} \
			{input.r1} \
			{input.r2} &> >(tee {log})"

	rule multiqc_pe:
		input:
			expand(rules.fastqc1_pe.output.dir, bname=readBasename),
			expand(rules.fastqc2_pe.output.dir, bname=readBasename),
			expand(rules.fastqscreen_pe.output.r1, bname=readBasename),
			expand(rules.fastqscreen_pe.output.r2, bname=readBasename),
		output:
			dir = directory("output/multiqc/"),
		log:
			"output/logfiles/multiqc.log"
		shell:
			"multiqc --outdir {output.dir} {input} &> >(tee {log})"

else:
	print("No R2 reads found, assuming single end input files")

	reads = glob.glob("inputs/*.fastq.gz")

	readBasename = []
	for read in reads:
		readBasename.append(os.path.basename(read.rsplit('.', -1)[0]))

	rule all:
		input:
			expand("inputs/{bname}.fastq.gz", bname=readBasename),
			"output/multiqc/",

	rule fastqc1_se:
		input:
			r1 = "inputs/{bname}.fastq.gz",
		output:
			dir = directory("output/pre_screen-{bname}"),
			flag = temp(touch("output/pre_screen-{bname}/flag")),
		log:
			"output/logfiles/{bname}-fastqc1.log"
		threads:
			4
		shell:
			"fastqc \
			-t {threads} \
			-o {output.dir} \
			{input.r1} &> >(tee {log})"

	rule fastqscreen_se:
		input:
			r1 = "inputs/{bname}.fastq.gz",
		output:
			dir = directory("output/fastq_screen-{bname}"),
			r1 = "output/fastq_screen-{bname}/{bname}.tagged_filter.fastq.gz",
		log:
			"output/logfiles/{bname}-fastq_screen.log"
		threads:
			4
		shell:
			"fastq_screen \
			--nohits \
			--threads {threads} \
			--conf /resources/fastq_screen.conf \
			--outdir {output.dir} \
			{input.r1} &> >(tee {log})"

	rule fastqc2_se:
		input:
			r1 = rules.fastqscreen_se.output.r1,
		output:
			dir = directory("output/post_screen-{bname}/{bname}"),
			flag = temp(touch("output/post_screen-{bname}/{bname}/flag")),
		log:
			"output/logfiles/{bname}-fastqc2.log"
		threads:
			4
		shell:
			"fastqc \
			-t {threads} \
			-o {output.dir} \
			{input.r1} &> >(tee {log})"

	rule multiqc_se:
		input:
			expand(rules.fastqc1_se.output.dir, bname=readBasename),
			expand(rules.fastqc2_se.output.dir, bname=readBasename),
			expand(rules.fastqscreen_se.output.r1, bname=readBasename),
		output:
			dir = directory("output/multiqc/"),
		log:
			"output/logfiles/multiqc.log"
		shell:
			"multiqc --outdir {output.dir} {input} &> >(tee {log})"
