## Hunt desensitize-wf-smk
Snakemake/docker workflow to remove sensitive human DNA from metagenomic samples

#### Workflow
Paired end workflow version:  
![workflow dag](.peDag.png)

Single end workflow version:  
![workflow dag](.seDag.png)

#### Container: Sparse software/ref overview*
| Tool/pipeline |  Version  | Note |
|----------|:-------------:|------:|
| BB-map | 38.86 | reformat.sh/repair.sh |
| Fastqc | 0.11.9 | - |
| Fastq_screen | 0.14.0 | Ref: hg38 (GRCh38) |
| Multiqc | 1.9 | - |

(*excluding  dependencies)
## Dependencies
Desensitize-wf-smk needs the following dependencies to run:
- Docker

#### Installation:  
Clone this repository:  
``git clone https://gitlab.com/oskarvid/desensitize-wf.git``  

Navigate to the cloned repository:  
``cd desensitize-wf``  

Build docker container  
(tag needs to be "hunt-smk" as this is referred to by the `scripts/start-workflow.sh` script. Total size is roughly 10GB):  
``docker build -t hunt-smk .``  

#### Usage:  
You start the workflow by running `./scripts/start-workflow.sh`. The `Snakefile` will look in the `inputs` directory for a file named `*R2.fastq.gz` and attempt to run in paired end mode, but if no R2 file is found it assumes that it should run in single end mode.  
**Note: The fastq.gz file suffix is hard coded at this time!**  
If you wish to set the number of cores, instead of using all cores by default, change `-j` to `-j <value>`.  
Results will be present in the `output/` folder, and includes hg38-screened and synced fastq files as well as a QC-report from multiqc.

## Known bugs
A question mark directory (literally named "?") is created when fastqc is executed, this directory contains a directory named .java, which in turn contains a directory named fonts. This is unecessary clutter and needs to fixed.

