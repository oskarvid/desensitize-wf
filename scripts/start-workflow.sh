#!/bin/bash

docker \
run \
--rm \
-u 1000:1000 \
-ti \
-v $(pwd):/data \
-w /data \
hunt-smk \
snakemake \
-j \
-p \
-r $@
